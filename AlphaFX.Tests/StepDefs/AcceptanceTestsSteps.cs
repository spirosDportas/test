﻿using AlphaFX.Domain;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using Xunit;

namespace AlphaFX.Tests.StepDefs
{
    [Binding]
    public class AcceptanceTestsSteps
    {
        public string InputText { get; private set; }
        public WordCounterProcessor WordProcessor { get; private set; }
        public long NumberOfWords { get; private set; }
        public CountedWordsResult Result { get; private set; }

        [Given(@"I have finite ""(.*)""")]
        public void GivenIHaveFinite(string text)
        {
            this.InputText = text;
        }
        
        [When(@"I calculate the number of words it contains")]
        public void WhenICalculateTheNumberOfWordsItContains()
        {
            this.WordProcessor = new WordCounterProcessor(new WordSplitter(), new WordCounter());
            this.Result = this.WordProcessor.ProcessText(this.InputText);
            this.NumberOfWords = this.Result.CountedWords.Count;
        }
        
        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(long expectedResult)
        {
            Assert.Equal(expectedResult, this.NumberOfWords);
        }

        [Given(@"I have the finite Text")]
        public void GivenIHaveFiniteText(Table table)
        {
            this.InputText = table.Rows[0].Values.First();
        }

        [Then(@"the top10 result should be")]
        public void ThenTheTopResultShouldBe(Table table)
        {
            var expected = table.Rows.ToDictionary(x => x.Values.First(), x => Int32.Parse(x.Values.Last()));
            Assert.Equal(expected, this.Result.Top10);
        }

    }
}
