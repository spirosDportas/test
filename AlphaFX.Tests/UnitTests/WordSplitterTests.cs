﻿using AlphaFX.Domain;
using System.Linq;
using System.Text;
using Xunit;

namespace AlphaFX.Tests.UnitTests
{
    public class WordSplitterTests
    {
        [Fact]
        public void SplitWordsReturnsNumberOfWordsFound()
        {
            //Arrange
            IWordSplitter sut = new WordSplitter();
            
            //Act
            var uniqueWordsFound = sut.SplitWords("Alexa ranks websites based on a combined measure of page views and unique site users");
            
            //Assert
            Assert.Equal(15, uniqueWordsFound.Count());
        }

        [Fact]
        public void SplitWordsWorksWithPunctuationCharacters()
        {
            //Arrange
            IWordSplitter sut = new WordSplitter();
            
            //Act
            var uniqueWordsFound = sut.SplitWords(@"Alexa,ranks websites,,,based//on a\\combined measure!of page.views and-unique(site)users");
            
            //Assert
            Assert.Equal(15, uniqueWordsFound.Count());
        }
    }
}
