﻿using AlphaFX.Domain;
using System;
using System.Collections.Generic;
using Xunit;

namespace AlphaFX.Tests.UnitTests
{
    public class WordCounterTests
    {
        [Fact]
        public void CountWordsReturnsUniqueNumberOfWordsFound()
        {
            //Arrange
            var inputData = new List<string>
                { "Alexa", "ranks", "websites", "based", "combined", "combined", "page" };
            var sut = new WordCounter();

            //Act
            var result = sut.CountWords(inputData);
            var actual = result.Count;
            
            //Assert
            Assert.Equal(6, actual);
        }

        [Fact]
        public void CountWordsReturnsUniqueWords()
        {
            //Arrange
            var inputData = new List<string>
                { "Alexa", "ranks", "websites", "based", "combined", "combined", "page" };
            var sut = new WordCounter();

            //Act
            var result = sut.CountWords(inputData);
            var actual = result.Keys;

            //Assert
            foreach (var word in inputData)
            {
                Assert.Contains(word, actual);
            }
        }

        [Fact]
        public void CountWordsReturnsDictionaryWithCounterPerWord()
        {
            //Arrange
            var inputData = new List<string>
                { "Alexa", "ranks", "websites", "based", "combined", "combined", "page" };
            var sut = new WordCounter();

            //Act
            var result = sut.CountWords(inputData);
            var actual = result["combined"];

            //Assert
            Assert.Equal(2, actual);
        }

        [Fact]
        public void DefaultStringComparisonIsInvariantCultureIgnoreCase()
        {
            //Arrange
            var inputData = new List<string>
                { "Alexa", "ranks", "websites", "based", "combined", "combined", "Combined", "page" };
            var sut = new WordCounter();

            //Act
            var result = sut.CountWords(inputData);
            var actual = result["combined"];

            //Assert
            Assert.Equal(3, actual);
        }

        [Fact]
        public void CountWordsUsesStringComparisonPassed()
        {
            //Arrange
            var inputData = new List<string>
                { "Alexa", "ranks", "websites", "based", "combined", "combined", "Combined", "page" };
            var sut = new WordCounter();

            //Act
            var result = sut.CountWords(inputData, StringComparer.InvariantCulture);
            var actual = result["combined"];

            //Assert
            Assert.Equal(2, actual);
        }
    }
}
