﻿namespace AlphaFX.Domain
{
    public class WordCounterProcessor
    {
        protected virtual IWordSplitter WordSplitter { get; }
        protected virtual IWordCounter WordCounter { get; }

        public WordCounterProcessor(IWordSplitter wordSplitter, IWordCounter wordCounter)
        {
            this.WordSplitter = wordSplitter;
            this.WordCounter = wordCounter;
        }

        public virtual CountedWordsResult ProcessText(string input)
        {
            var wordsList = this.WordSplitter.SplitWords(input);
            var countedWords = this.WordCounter.CountWords(wordsList);
            var result = new CountedWordsResult(countedWords);
            return result;
        }
    }
}