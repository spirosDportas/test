﻿using System.Collections.Generic;
using System.Linq;

namespace AlphaFX.Domain
{
    public class CountedWordsResult
    {
        protected Dictionary<string, int> countedWords;
        public virtual Dictionary<string, int> CountedWords => countedWords.ToDictionary(x => x.Key, x => x.Value);

        public Dictionary<string, int> Top10 => countedWords
            .OrderByDescending(x => x.Value)
            .Take(10)
            .ToDictionary(x => x.Key, x => x.Value);

        public CountedWordsResult(Dictionary<string, int> input)
        {
            countedWords = input;
        }
    }
}