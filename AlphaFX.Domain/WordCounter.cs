﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AlphaFX.Domain
{
    public class WordCounter : IWordCounter
    {
        public virtual Dictionary<string, int> CountWords(IEnumerable<string> inputWords, StringComparer stringComparer = null)
        {
            if(stringComparer == null)
            {
                stringComparer = StringComparer.InvariantCultureIgnoreCase;
            }
            var counters = inputWords.Distinct(stringComparer).ToDictionary(x => x, x => 0);
            var keys = counters.Keys.ToList();
            foreach (var key in keys)
            {
                counters[key] = inputWords.Count(x => stringComparer.Compare(x, key) == 0);
            }
            return counters;
        }
    }
}