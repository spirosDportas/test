﻿using AlphaFX.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace AlphaFX.Tests.UnitTests
{
    public class CountedWordsResultTests
    {
        [Fact]
        public void Top10ReturnsUpTo10Words()
        {
            //Arrange
            var inputData = new List<string>
                { "Alexa", "ranks", "websites", "based", "combined", "combined2", "combined3", "combined4", "combined5", "page" }
                .ToDictionary(x => x, x => 1);
            var sut = new CountedWordsResult(inputData);

            //Act
            var actual = sut.Top10;

            //Assert
            Assert.True(actual.Count() < 11);
        }

        [Fact]
        public void Top10ReturnsSortedDescResult()
        {
            //Arrange
            var inputData = new List<string>
                { "Alexa", "ranks", "websites", "based", "combined", "combined2", "combined3", "combined4", "combined5", "page" }
                .ToDictionary(x => x, x => 1);
            inputData["ranks"] = 10;
            inputData["based"] = 11;
            inputData["combined3"] = 3;

            var sut = new CountedWordsResult(inputData);

            //Act
            var actual = sut.Top10;

            //Assert
            Assert.Equal("based", actual.First().Key);
            Assert.Equal("ranks", actual.Skip(1).First().Key);
            Assert.Equal("combined3", actual.Skip(2).First().Key);
        }
    }
}
