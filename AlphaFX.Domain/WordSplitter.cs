﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlphaFX.Domain
{
    public class WordSplitter : IWordSplitter
    {
        public const char Space = ' ';

        public virtual IEnumerable<string> SplitWords(string input)
        {
            var cleanedInput = this.GetPreparedStringForSplitting(input);
            var words = cleanedInput.Split(Space)
                .Where(x => !string.IsNullOrEmpty(x))
                .ToList();
            return words;
        }

        protected virtual string GetPreparedStringForSplitting(string input)
        {
            var stringBuilder = new StringBuilder(input.Length);
            foreach(var inputChar in input)
            {
                var filteredChar = char.IsPunctuation(inputChar) ? Space : inputChar;
                stringBuilder.Append(filteredChar);
            }
            var result = stringBuilder.ToString();
            return result;
        }
    }
}