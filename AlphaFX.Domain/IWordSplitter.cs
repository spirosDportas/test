﻿using System.Collections.Generic;

namespace AlphaFX.Domain
{
    public interface IWordSplitter
    {
        IEnumerable<string> SplitWords(string v);
    }
}