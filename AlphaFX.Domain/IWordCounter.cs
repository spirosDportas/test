﻿using System;
using System.Collections.Generic;

namespace AlphaFX.Domain
{
    public interface IWordCounter
    {
        Dictionary<string, int> CountWords(IEnumerable<string> inputWords, StringComparer stringComparer = null);
    }
}